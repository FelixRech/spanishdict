# For parsing the scraped website
from lxml import html
# For checking whether downloaded HTML exists
import os.path
# For downloading the page from spanishdict
import requests

class Conjugation:
    verb = ""
    etree = None
    conjugations = {}

    def __init__(self, verb):
        self.verb = verb
        self.createETree()
        self.getIndicativePresent()
        self.getPresentPerfect()
        self.getSimplePast()

    def createETree(self):
        if self.checkForDownloadedHTML():
            print("Using html file")
            self.loadFromHTML()
        else:
            print("Using web download")
            self.scrapeWebsite()

    def checkForDownloadedHTML(self):
        return os.path.isfile("html/conjugations/" + self.verb + ".html")

    def loadFromHTML(self):
        self.etree = html.parse("html/conjugations/" + self.verb + ".html")

    def scrapeWebsite(self):
        baseURL = "https://www.spanishdict.com/conjugate/"
        verbURL = baseURL + self.verb
        webpage = requests.get(verbURL)
        with open("html/conjugations/" + self.verb + ".html",'w+') as output:
            output.write(webpage.text)
            output.close()
        self.loadFromHTML()

    def getIndicativePresent(self):
        self.conjugations["indicative-present"] = self.getTupleFromEtree(0, 1)

    def getPresentPerfect(self):
        self.conjugations["present-perfect"] = self.getTupleFromEtree(4, 1)

    def getSimplePast(self):
        self.conjugations["simple-past"] = self.getTupleFromEtree(0, 2)

    def getTupleFromEtree(self, table, tr):
        table = self.etree.xpath("//table")[table]
        return [table[i][tr].text_content() for i in range(1, 7)]

if __name__ == "__main__":
    print("A little debug presentation - conjugations of olvidar:")
    c = Conjugation("olvidar")
    print("\nIndicative present: " + str(c.conjugations["indicative-present"]))
    print("\nPresent perfect: " + str(c.conjugations["present-perfect"]))
    print("\nSimple past: " + str(c.conjugations["simple-past"]))
