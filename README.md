# Spanishdict
This is a small python program to access Spanishdict.com's spanish dictionary. First and foremost this project is going to allow access to conjugations with translations and other information as possible future goals.

## Disclaimer
This project is not in any way, shape or form connected to spanishdict.com.

Therefore any use of this program is not officially allowed by spanishdict.com - it is actually prohibited in the terms of service (http://www.spanishdict.com/company/tos - 6. Intellectual Property Rights).

Please be aware that using this program might break applicable law. Any liability lies with the user.

Future versions will allow users to minimize traffic with Spanishdict and download each scraped page only once.
